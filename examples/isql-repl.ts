import { OSsocketAdapter } from '../src/OSsocketAdapter';
import * as readline from 'node:readline';
import { ExitStatus } from 'typescript';
import { EIP, ExecuteParams } from '../src/client';
import * as eip from "../src/eip"
import { stdin as input, stdout as output } from 'node:process';


class ISQl extends EIP {

    constructor(port) {
        super();
        this.adapter = new OSsocketAdapter(this.handleResponse.bind(this), port);
    }

    onExecute(req: eip.ExecuteRequest, resp: eip.ExecuteResponse): void
    {
        console.log(resp);
    }
    onJump(req: eip.JumpRequest, resp: eip.JumpResponse): void
    {}
    onRevert(req: eip.RevertRequest, resp: eip.RevertResponse): void {}
    onDeref (req: eip.DerefRequest, resp: eip.DerefResponse): void {}
    onExecutionTree(req: eip.ExecutionTreeRequest, resp: eip.ExecutionTreeResponse): void
    {}
    onTrace(req: eip.TraceRequest, resp: eip.TraceResponse): void
    {}
    onPath(req: eip.PathRequest, resp: eip.PathResponse): void
    {}
    onCurrentReference(req: eip.CurrentReferenceRequest, resp: eip.CurrentReferenceResponse): void
    {}
    onAllReferences(req: eip.AllReferencesRequest, resp: eip.AllReferencesResponse): void
    {}
    onLeaves(req: eip.LeavesRequest, resp: eip.LeavesResponse): void
    {}
    onMeta(req: eip.MetaRequest, resp: eip.MetaResponse): void
    {}

    onUnkownError(resp: eip.ResponseMessage): void
    {}
    onError(req: eip.RequestMessage, resp: eip.ResponseMessage): void
    {}
}

var args = process.argv.slice(2);

if (args.length == 0) {
    console.log("usage: isql-repl <server-port>");
    process.exit(1);
}

var port = args[0];
let client = new ISQl(port);


const rl = readline.createInterface({ input, output });

rl.on('SIGINT', () => {
    process.exit(0);
});

function repl() {
    rl.question('iSQL> ', (program) => {
        client.execute(new ExecuteParams(program));
        console.log("Handled");
        repl();
    });
}


repl();