
import {Parser} from './Parser';

const { spawn } = require("child_process");

const httpServer = require("http").createServer();
const net = require("net")

const io = require("socket.io")(httpServer, {
    cors: {
        origin: "*"
    }
});

export function listen(port_left, port_right) {
    httpServer.listen(port_left, () =>
    console.log(`server listening at http://localhost:${port_left}`)
  );

  io.use((_socket, next) => {
    next();
  });


  io.on("connection", (socket) => {
    let client  = new net.Socket();
    let p = new Parser();
    client.setEncoding("utf8");
    client.connect({
        port:port_right
    });
    setTimeout(() => socket.disconnect(true), 900000); // For now a 15 minute timeout.

    socket.on("command", (command) => {
        client.write("Content-Length:" + command.length + "\r\nContent-Type: jrpcei\r\n\r\n" + command);
    });

    socket.on("disconnect", () => {
        console.log("Client disconnected");
    });

    client.on("data", function (data) {
        let res = p.parse(data);
        res?.forEach((value, i, a) => {
            socket.emit("data", value);
        });
    });
  });
}