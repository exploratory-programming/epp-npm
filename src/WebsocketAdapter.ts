import { ClientAdapter } from "./AbstractClientAdapter";
import { io, Socket } from "socket.io-client";



export class WebsocketAdapter extends ClientAdapter {
    socket: Socket;

    constructor(callback, socket: Socket) {
        super(callback);
        this.socket = socket;
        this.socket.on("data", (data) => {
            this.callback(data);
        })
    }

    send(payload: string) {
        this.socket.emit("command", payload);
    }
}