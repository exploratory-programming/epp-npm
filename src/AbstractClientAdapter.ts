

export abstract class ClientAdapter
{
    callback: any;
    abstract send(payload: string);

    constructor (fn) {
        this.callback = fn
    }
}