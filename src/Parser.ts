


export class Parser {
    to_parse: number; 
    processed_data: string;

    constructor() {
        this.to_parse = 0;
        this.processed_data = "";
    }

    private parseContinuation(data: string)
    {
        if (data.length < this.to_parse) {
            this.processed_data += data;
            this.to_parse -= data.length; 
            return null;
        } else {
            this.processed_data += data.substring(0, this.to_parse);
            data = data.substring(this.to_parse);
            this.to_parse = 0;
            let result = this.processed_data;
            this.processed_data = "";
            return [result];
        }     
    }

    private parseNewMessage(data: string)
    {
        let res: string[] = [];
        while (data.length > 0) {
                if (!data.includes('\r\n\r\n')) {
                    // Error?
                    return;
                }
                let data_split = data.split('\r\n', 3);
                var ln = parseInt(data_split[0].split(":")[1]);
                var header = data_split.join('')
                let body = data.slice(header.length + 6)
                if (ln > body.length) {
                    this.processed_data = body;
                    this.to_parse = ln - body.length;
                    return res;
                } else {
                    if (body.length > ln) {
                        let parsed_result = body.slice(0, ln);
                        let rem = body.slice(ln);
                        data = rem;
                        res.push(parsed_result);
                    } else {
                        res.push(body);
                        data = "";
                    }
                }
        }
        return res;
    }

    parse(data: string) {
        if (this.to_parse !== 0) {
            return this.parseContinuation(data);
        } else {
            return this.parseNewMessage(data);
        }
    }
}