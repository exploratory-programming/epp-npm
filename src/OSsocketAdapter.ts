import { ClientAdapter } from "./AbstractClientAdapter";
const net = require("net")
import {Parser} from "./Parser";


export class OSsocketAdapter extends ClientAdapter {
    socket;
    parser : Parser;

    constructor(callback, port, host='0.0.0.0') {
        super(callback);

        this.parser = new Parser();
        this.socket = new net.Socket();
        this.socket.setEncoding("utf8");
        this.socket.connect(port, host);
        this.socket.on("data", (data) => { 
            this.receive(data);
        });
    }

    receive(data: string) {
        let res = this.parser.parse(data);
        res?.forEach( (v, i, a) => {
            this.callback(v);
        })
    }

    send(payload: string) {
        let message = "Content-Length:" + payload.length + "\r\nContent-Type: jrpcei\r\n\r\n" + payload; 
        this.socket.write(message);
    }
}