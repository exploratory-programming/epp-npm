"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var globals_1 = require("@jest/globals");
var Parser_1 = require("../src/Parser");
var p;
(0, globals_1.beforeEach)(function () {
    p = new Parser_1.Parser();
});
function buildMessage(content) {
    return "Content-Length:" + content.length + "\r\nContent-Type: jrpcei\r\n\r\n" + content;
}
(0, globals_1.test)('Full message should be correct', function () {
    var body = '{"jsonrpc": "2.0", "id": 1, "method": "textDocument/completion"}';
    var m = buildMessage(body);
    var res = p.parse(m);
    (0, globals_1.expect)(res.length).toBe(1);
    (0, globals_1.expect)(res[0]).toBe(body);
    (0, globals_1.expect)(p.to_parse).toBe(0);
});
(0, globals_1.test)('Partial message should be correct', function () {
    var body = '{"jsonrpc": "2.0", "id": 1, "method": "textDocument/completion"}';
    var m = buildMessage(body);
    var part_m = m.slice(0, m.length / 2);
    var part_m2 = m.slice(m.length / 2);
    p.parse(part_m);
    var res = p.parse(part_m2);
    (0, globals_1.expect)(res.length).toBe(1);
    (0, globals_1.expect)(res[0]).toBe(body);
    (0, globals_1.expect)(p.to_parse).toBe(0);
});
(0, globals_1.test)('Multiple messages in one payload', function () {
    var body = '{"jsonrpc": "2.0", "id": 1, "method": "textDocument/completion"}';
    var body2 = '{"jsonrpc": "2.0", "id": 2, "method": "textDocument/completion"}';
    var m = buildMessage(body);
    var m2 = buildMessage(body2);
    var res = p.parse(m + m2);
    (0, globals_1.expect)(res.length).toBe(2);
    (0, globals_1.expect)(res[0]).toBe(body);
    (0, globals_1.expect)(res[1]).toBe(body2);
    (0, globals_1.expect)(p.to_parse).toBe(0);
});
(0, globals_1.test)('One full message and one partial message', function () {
    var body = '{"jsonrpc": "2.0", "id": 1, "method": "textDocument/completion"}';
    var body2 = '{"jsonrpc": "2.0", "id": 2, "method": "textDocument/completion"}';
    var m = buildMessage(body);
    var m2 = buildMessage(body2);
    var part_m2_1 = m2.slice(0, m2.length / 2);
    var part_m2_2 = m2.slice(m2.length / 2);
    var res = p.parse(m + part_m2_1);
    (0, globals_1.expect)(res.length).toBe(1);
    (0, globals_1.expect)(res[0]).toBe(body);
    (0, globals_1.expect)(p.to_parse).toBe(part_m2_2.length);
    var new_res = p.parse(part_m2_2);
    (0, globals_1.expect)(new_res[0]).toBe(body2);
});
//# sourceMappingURL=parser.test.js.map