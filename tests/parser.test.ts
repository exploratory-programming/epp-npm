import {describe, beforeEach, expect, test} from '@jest/globals';
import {Parser} from '../src/Parser';

var p;

beforeEach(() => {
    p = new Parser();
});


function buildMessage(content: string) 
{
    return "Content-Length:" + content.length + "\r\nContent-Type: jrpcei\r\n\r\n" + content;

}


test('Full message should be correct', () => {
    let body = '{"jsonrpc": "2.0", "id": 1, "method": "textDocument/completion"}'
    let m = buildMessage(body);
    let res = p.parse(m);
    expect(res.length).toBe(1);
    expect(res[0]).toBe(body);
    expect(p.to_parse).toBe(0);
});


test('Partial message should be correct', () => {
    let body = '{"jsonrpc": "2.0", "id": 1, "method": "textDocument/completion"}'
    let m = buildMessage(body);
    let part_m = m.slice(0, m.length / 2);
    let part_m2 = m.slice(m.length / 2);
    p.parse(part_m);
    let res = p.parse(part_m2);
    expect(res.length).toBe(1);
    expect(res[0]).toBe(body);
    expect(p.to_parse).toBe(0);
});

test('Multiple messages in one payload', () => {
    let body = '{"jsonrpc": "2.0", "id": 1, "method": "textDocument/completion"}'
    let body2 = '{"jsonrpc": "2.0", "id": 2, "method": "textDocument/completion"}'
    let m = buildMessage(body);
    let m2 = buildMessage(body2);
    let res = p.parse(m + m2);
    expect(res.length).toBe(2);
    expect(res[0]).toBe(body);
    expect(res[1]).toBe(body2);
    expect(p.to_parse).toBe(0);
})

test('One full message and one partial message', () => {
    let body = '{"jsonrpc": "2.0", "id": 1, "method": "textDocument/completion"}'
    let body2 = '{"jsonrpc": "2.0", "id": 2, "method": "textDocument/completion"}'
    let m = buildMessage(body);
    let m2 = buildMessage(body2);
    let part_m2_1 = m2.slice(0, m2.length / 2);
    let part_m2_2 = m2.slice(m2.length / 2);
    let res = p.parse(m + part_m2_1);
    expect(res.length).toBe(1);
    expect(res[0]).toBe(body);
    expect(p.to_parse).toBe(part_m2_2.length);
    let new_res = p.parse(part_m2_2);
    expect(new_res[0]).toBe(body2);
})
