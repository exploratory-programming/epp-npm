
ISQL_PORT := 5002

.PHONY: isql-repl
isql-repl: build
	node dist/src/isql-repl.js ${ISQL_PORT}

.PHONY: build
build:
	yarn run tsc

.PHONY: test
test: 
	yarn jest 

.PHONY: pack
pack: clean
	make build
	npm pack

.PHONY: publish
publish: clean
	make build
	npm publish --access public


.PHONY: clean
clean:
	rm -rf ./dist
	rm -f *.tgz
