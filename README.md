
# Installation

[nix](https://nix.dev/tutorials/install-nix) is required for building.

# npm package
The implementation is available as an NPM package [here](https://www.npmjs.com/package/@exploratory-programming/epp)

# Documentation
For now, the documentation is rather minimal.
The examples folder contains an simple example using the OS sockets.
Alternatively, one can use a websocket. When using the websocket, the websocket bridge is needed 
to communicate between the client and a server.

All interfaces must extend the abstract EIP class, which is used to send and receive EPP messages.
